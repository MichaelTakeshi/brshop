#encoding: utf-8
require 'rubygems'
require 'sinatra'
require 'sinatra/reloader'
require 'pry'
require 'sqlite3'

def is_barber_exists?(db, name)
	db.execute('select * from Barbers where name=?', [name]).length > 0
end

def seed_db(db, barbers)
	barbers.each do |barber|
		if !is_barber_exists? db, barber
			db.execute 'insert into Barbers (name) values (?)',[barber]
		end
	end
end

def get_db
	return SQLite3::Database.new 'brshop.db'

	return db
end

before do
	db = get_db
	@barbers = db.execute 'select * from Barbers'
end

configure do
	db = get_db
	db.execute 'CREATE TABLE IF NOT EXISTS
	 "Users"
	  (
			"Id" INTEGER PRIMARY KEY AUTOINCREMENT,
			"Name" VARCHAR,
			"Email" TEXT,
			"Phone" TEXT,
			"Date" TEXT,
			"Time" TEXT,
			"Barber" TEXT,
			"Color" TEXT
			)'
	db.execute 'CREATE TABLE IF NOT EXISTS
	 "Barbers"
	  (
			"Id" INTEGER PRIMARY KEY AUTOINCREMENT,
			"Name" TEXT
			)'
		seed_db(
			db,
			['Jessie Pinkman', 'Walter White', 'Gus Fring', 'Alex Koleso', 'Ragnar Lodbrock', 'Mike Ehrmantaut']
		)

end


get '/' do
	 erb 'Welcome to Michaels Masters test'
end

get '/about' do
	erb :about
end

get '/visit' do
	erb :visit
end

post '/visit' do

	@title = 'Thank you!'
	@user_name = params[:user_name]
	@user_mail = params[:user_mail]
	@user_phone = params[:user_phone]
	@user_date = params[:user_date]
	@user_time = params[:user_time]
	@barber1 = params[:barber1]
	@color = params[:color]
	@message = "Dear #{@user_name}, we will send a message to you at #{@user_mail} and #{@user_phone}. Your color #{@color}
	to remind you of your haircut on #{@user_date} at #{@user_time}"

	hh = { :user_name => 'Enter your name!',
		:user_mail => 'Enter your mail!',
		:user_phone => 'Enter your phone!',
		:user_date => 'Enter date!',
		:user_time => 'Enter time!',
		:barber1 => 'Choose Manager',
		:color => 'Choose color'}


	@error = hh.select {|key,_| params[key] == ""}.values.join(", ")
	if @error != ''
		return erb :visit
	end


	db = get_db
	db.execute 'insert into
	Users
	(
	name,
	phone,
	email,
	date,
	time,
	barber,
	color
	)
	values (?,?,?,?,?,?,?)', [@user_name, @user_phone, @user_mail,  @user_date, @user_time, @barber1, @color]



	# f = File.open './users.txt', 'a'
  # f.write "User: #{@user_name}, Mail: #{@user_mail}, Phone: #{@user_phone}, Date: #{@user_date}, Time: #{@user_time}, MasterBarber: #{@barber1}, Color: #{@color} \n"
  # f.close
	erb :message
end

post '/' do
end

get '/contacts' do
	erb :contacts
end

post '/contacts' do

	@user_text = params[:user_text]
	@name = params[:name]
	@mail = params[:mail]

	hh = {:name => 'Enter your name!',
		    :mail => 'Enter your mail!',
		    :user_text => 'Type text to contact with us!',

	}

	@error = hh.select {|key,_| params[key] == ""}.values.join(", ")
	if @error != ''
		return erb :contacts
	end

	f = File.open '/home/takeshi/Project/24les/public/contacts.txt', 'a'
  f.write "Name: #{@name}, Email: #{@mail}, Text: #{@user_text}, \n"
	f.close

	erb :contacts
end

get '/admin' do
  erb :admin
end

post '/admin' do
  admin_login = params[:admin_login]
  admin_password = params[:admin_password]
  if admin_login == 'admin' && admin_password == 'secret'
    f = File.open '/home/takeshi/Project/23les/users.txt', 'r'
    @info = f.read
    f.close
    erb :admintrue
  else
    'Access denied'
  end
end

get '/showusers' do

	db = get_db
	db.results_as_hash = true
	@results = db.execute 'select * from Users order by id desc'

	erb :showusers
end
